using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class MapIndicatorChange : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.transform.root.GetComponent<PhotonView>().IsMine)
        {
            gameObject.GetComponent<Renderer>().material.SetColor("_Color",Color.white);
            gameObject.GetComponent<Renderer>().material.SetColor("_EmissionColor",Color.white);
        } 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
