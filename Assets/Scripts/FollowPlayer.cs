using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private GameObject Player;
   
    // Start is called before the first frame update
    void Start()
    {
        
        

        
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void LateUpdate()
    {
        transform.position = Player.transform.position + new Vector3(0, 6, 1);
        transform.LookAt(transform.position + Camera.main.transform.forward);
    }
}
