using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    private void OnLevelWasLoaded(int level)
    {
        if (level != 2)
        {
            if (this.gameObject.GetPhotonView() != null)
            {
                PhotonNetwork.Destroy(this.gameObject);

            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }
    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if (GameLength.gameEnd)
        {
            if (this.gameObject.GetPhotonView() != null)
            {
                PhotonNetwork.Destroy(this.gameObject);

            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }
}
