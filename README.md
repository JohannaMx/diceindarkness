Ein Projekt von Tobias Behn, Jonas Christmann, Johanna Meyer, André Schuldt und Bennedict Schweimer.
Entstanden als Ergebnis des Moduls "Projekt B" mit dem Titel "Konzeptionalisierung und Entwicklung eines kompetetiven Multiplayer-Onlinespiels in Unity" an der HAW-Hamburg.

## Description
Kompetetives Online-Multiplayer-Spiel. Es besteht eine Auswahl aus 4 Fantasy-Charakteren, die von typischen Pen-and-Paper-Rollenspielen inspiriert sind. Bis zu 8 Spieler treten in einem physikbasierten Kampfsystem gegeneinander an. Die Spielwelt wird aus vier (von sechs) zufällig gewählten und rotierten Spielbrettern zusammengesetzt. Der Spieler, der nach Ablauf der Zeit die meisten Punkte hat, gewinnt.
Heilung erfolgt mit Linksklick, Rotation während gedrückter rechter Maustaste und entsprechender "Drehung" der Maus.

## Visuals

Gameplay Trailer:

[![](http://img.youtube.com/vi/yvjBy1AotgY/0.jpg)](http://www.youtube.com/watch?v=yvjBy1AotgY "Gameplay Trailer Dice In Darkness")

