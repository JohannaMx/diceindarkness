using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.AI;

public class TestMove : MonoBehaviour
{
    public PhotonView view;
    private float moveSpeed = 20;
    private NavMeshAgent agent;

    private void Start()
    {
        view = GetComponent<PhotonView>();
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if (view.IsMine)
        {
            
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");

            Vector3 movementDirection = new Vector3(horizontalInput, 0, verticalInput);
            movementDirection.Normalize();

            agent.SetDestination(movementDirection);
        
            transform.Translate(movementDirection * moveSpeed * Time.deltaTime, Space.World);

           
        

          
        }


    }

  


}
