using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class CreateAndJoinRooms : MonoBehaviourPunCallbacks
{

    public TMP_InputField createInput;
    public TMP_InputField joinInput;
    public TMP_InputField name;
    [SerializeField] private int roomsize;

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

    }
    public void SaveNickName()
    {
        string nickName = name.text.ToString();

        if (nickName == "")
        {
            nickName = "Unnamed# " + Random.Range(1, 5).ToString();
        }

        PhotonNetwork.player.NickName = nickName;

        
    }
    public void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)roomsize };
        PhotonNetwork.CreateRoom(createInput.text, roomOptions);
    }

    public void JoinRoom()
    {
        PhotonNetwork.JoinRoom(joinInput.text);
        
    }

    public override void OnJoinedRoom()
    {
        CanvasFader.faded = true;
        SceneManager.LoadScene("WaitingLobby");
    }


}
