using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class DelayStartLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject delayStartButton;
    [SerializeField] private GameObject delayEndButton;
    [SerializeField] private int roomsize;


    // Start is called before the first frame update
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        delayStartButton.SetActive(true);
    }

    public void DelayStart()
    {
        delayStartButton.SetActive(false);
        delayEndButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        CreateRoom();
    }

    void CreateRoom()
    {
        int randomRoomNumber = Random.Range(0, 100);
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen =true, MaxPlayers =(byte) roomsize};
        PhotonNetwork.CreateRoom("Room" + randomRoomNumber,roomOptions);

    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to create Room...server is trying so damn hard ");
    }

    public void DelayCancel()
    {
        delayEndButton.SetActive(false);
        delayStartButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }
   
}
