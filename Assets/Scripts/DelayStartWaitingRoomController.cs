using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class DelayStartWaitingRoomController : MonoBehaviourPunCallbacks
{
    private PhotonView view;
    [SerializeField] private int multiplayerSceneIndex;
    [SerializeField] private int menueSceneIndex;
    [SerializeField] private int maxPlayers;
    private int playercount;
    private int roomsize;
    [SerializeField] private int minPlayerstoStart;
    [SerializeField] private TMP_Text playerCountDisplay;
    [SerializeField] private TMP_Text timertoStartDisplay;


    private bool readyToCountDown;
    private bool readyToStart;
    private bool startingGame;

    //timer
    public static float timerToStartGame;
    private float notFullgameTimer;
    private float fullGameTimer;
    [SerializeField] private float maxWaitTime;
    [SerializeField] private float maxFullGameWaitTime;

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.CurrentRoom.MaxPlayers = (byte)maxPlayers;
        view = GetComponent<PhotonView>();
        fullGameTimer = maxFullGameWaitTime;
        notFullgameTimer = maxWaitTime;
        timerToStartGame = maxWaitTime;
        PlayerCountUpdtate();

        print(CanvasFader.faded);
    }
  

    void PlayerCountUpdtate()
    {
        playercount = PhotonNetwork.playerList.Length;
        roomsize = PhotonNetwork.CurrentRoom.MaxPlayers;
        Debug.Log(PhotonNetwork.CurrentRoom.MaxPlayers);
        playerCountDisplay.text = playercount + "/" + roomsize; 

        if(playercount == roomsize)
        {
            readyToStart = true;    
        }
        else if(playercount >= minPlayerstoStart)
        {
            readyToCountDown = true;
        }
        else
        {
            readyToCountDown = false;
            readyToStart = false;
        }

    }

    public override void OnPlayerEnteredRoom(PhotonPlayer newPlayer)
    {
        PlayerCountUpdtate();

        if (PhotonNetwork.IsMasterClient)
        {
            view.RPC("RPC_SendTimer", RpcTarget.Others, timerToStartGame);
        }
    }

    public override void OnPlayerLeftRoom(PhotonPlayer otherPlayer)
    {
        PlayerCountUpdtate();
    }

    [PunRPC]
    private void RPC_SendTimer(float timeIn)
    {
        timerToStartGame = timeIn;
        notFullgameTimer = timeIn;
        if (timeIn < fullGameTimer)
        {
            fullGameTimer = timeIn; 
        }
    }

    // Update is called once per frame
    void Update()
    {
        WaitingForMorePlayer();

    }

    void WaitingForMorePlayer()
    {
        if (playercount <=0)
        {
            ResetTimer();
        }

        if (readyToStart)
        {
            fullGameTimer -= Time.deltaTime;
            timerToStartGame = fullGameTimer;
        }else if (readyToCountDown)
        {
            notFullgameTimer -= Time.deltaTime;
            timerToStartGame = notFullgameTimer;
        }
        string tempTimer = string.Format("{0:00}", timerToStartGame);
        timertoStartDisplay.text = tempTimer;

        if (timerToStartGame <= 0f)
        {
            if (startingGame)
            
                return;
            StartGame();
            
        }
    }

    void ResetTimer()
    {
        timerToStartGame = maxWaitTime;
        notFullgameTimer = maxWaitTime;
        fullGameTimer = maxFullGameWaitTime;
    }


    //[PunRPC]
    //public void StartLevel(string name)
    //{
    //    PhotonNetwork.LoadLevel(name);
    //}
    void StartGame()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        startingGame = true;
        PhotonNetwork.CurrentRoom.IsOpen = false;
        //if (view.IsMine)
        //{
        //    view.RPC("StartLevel", RpcTarget.All, "SampleScene");
        //}
        if (PhotonNetwork.IsMasterClient)
        {
            StartCoroutine(LoadLevelAsync());
        }


    }

    IEnumerator LoadLevelAsync()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.LoadLevel("SampleScene");

        while (PhotonNetwork.LevelLoadingProgress < 1)
        {
            //Bei allen bei 100%
            Debug.Log("Loading: %" + (int)(PhotonNetwork.LevelLoadingProgress * 100) );
            yield return new WaitForEndOfFrame();
        }
    }


    public void DelayCancel()
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene("Lobby");
    }


}
