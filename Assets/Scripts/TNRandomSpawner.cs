using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class TNRandomSpawner : MonoBehaviour
{

	public string spawnPointTag = "Item";
	public bool alwaysSpawn = true;
	private bool oneTime = true;
	public List<GameObject> prefabsToSpawn;

	// Start is called before the first frame update
	void Start()
	{
		
	}

    private void Update()
    {
        if (PhotonNetwork.IsMasterClient && MapGenerator.playersLoaded && oneTime)
        {
			SpawnItems();
			oneTime = false;
        }
    }

    public void SpawnItems()
    {
		GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag(spawnPointTag);
		foreach (GameObject spawnPoint in spawnPoints)
		{
			int randomPrefab = Random.Range(0, prefabsToSpawn.Count);
			if (alwaysSpawn)
			{
				PhotonNetwork.Instantiate(prefabsToSpawn[randomPrefab].name, spawnPoint.transform.position, Quaternion.identity);
			}
			else
			{
				int spawnOrNot = Random.Range(0, 2);
				if (spawnOrNot == 0)
				{
					PhotonNetwork.Instantiate(prefabsToSpawn[randomPrefab].name, spawnPoint.transform.position, Quaternion.identity);
				}
				else
                {

					//StartCoroutine(RespawnItems(prefabsToSpawn[randomPrefab].name, spawnPoint.transform.position));
                }
			}
		}
	}

	IEnumerator RespawnItems(string name, Vector3 point)
    {

		yield return new WaitForSeconds(GameLength.gameLength/2);
		PhotonNetwork.InstantiateRoomObject(name, point, Quaternion.identity);
	}

    

}

