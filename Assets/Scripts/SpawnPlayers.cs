using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using System.Linq;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using TMPro;

public class SpawnPlayers : MonoBehaviour
{
    public GameObject[] playerPrefabs;

    public Transform[] spawnPoints;

    public TMP_Text timertext;
    public float timetoSpawn, spawnDelay;

    bool isRespawn = false;

    public static int playersSpawned = 0;

    public void Start()
    {

        timertext.enabled = false;
        Spawn(false);


    }


    private void Update()
    {
        RespawnTimer();
        
    }

    public void Spawn(bool rand)
    {

        GameObject.Find("DeathCam").GetComponent<Camera>().enabled = false;

        int index = (int)PhotonNetwork.player.CustomProperties["skin"];

        int spawnIndex;

        if (rand)
        {
            spawnIndex = Random.Range(0, spawnPoints.Length);
        }
        else
        {
            spawnIndex = PhotonNetwork.player.ActorNumber - 1;
        }

        if (PlayerController.LocalPlayerInstance == null)
        {
            PhotonNetwork.Instantiate(playerPrefabs[index].name, spawnPoints[spawnIndex].position, Quaternion.identity);
        } else
        {
            Debug.Log("Already Spawned");
        }

    }


    public void Respawn()
    {
        StartCoroutine("WaitForRespawn");
        isRespawn = true;
        timetoSpawn = spawnDelay;
        timertext.enabled = true;

    }

    IEnumerator WaitForRespawn()
    {
        yield return new WaitForSeconds(5);
        Spawn(true);
        timertext.enabled = false;
        isRespawn = false;
    }

    void RespawnTimer()
    {
        if (isRespawn)
        {
            timetoSpawn -= Time.deltaTime;
            timertext.text = timetoSpawn.ToString("0.0");
        }
    }


}
