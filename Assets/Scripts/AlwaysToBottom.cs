using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class AlwaysToBottom : MonoBehaviour
{
    public VisualEffect blood;
    void Update()
    {
        transform.rotation = Quaternion.identity;
        blood.SetVector3("StartPosition", transform.position);
    }
}
